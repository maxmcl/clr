use std::fmt;

use std::fs::File;
use std::io::{stdin, BufReader, Read};
use std::path::Path;

use clap::{App, Arg};

type HeadResult<T> = Result<T, Box<dyn std::error::Error>>;

#[derive(Debug)]
pub enum HeadError {
    InvalidNumLines { n_lines: String },
    InvalidNumBytes { n_bytes: String },
    InvalidCombination,
}

impl std::error::Error for HeadError {}

impl fmt::Display for HeadError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::InvalidNumLines { n_lines } => {
                write!(f, "illegal line count -- {}", n_lines)
            }
            Self::InvalidNumBytes { n_bytes } => {
                write!(f, "illegal byte count -- {}", n_bytes)
            }
            Self::InvalidCombination => {
                write!(
                    f,
                    "The argument '--lines <LINES>' cannot be \
               used with '--bytes <BYTES>'"
                )
            }
        }
    }
}

#[derive(Debug)]
pub struct Config {
    files: Vec<String>,
    n_lines: Option<usize>,
    n_bytes: Option<usize>,
}

impl Config {
    pub fn get_args() -> HeadResult<Config> {
        let matches = App::new("headr")
            .arg(
                Arg::new("n_lines")
                    .short('n')
                    .long("lines")
                    .takes_value(true),
            )
            .arg(
                Arg::new("n_bytes")
                    .short('c')
                    .long("bytes")
                    .takes_value(true),
            )
            .arg(Arg::new("files").default_value("-").min_values(1))
            .get_matches();
        let mut n_lines = match matches.value_of("n_lines") {
            None => None,
            Some(n_lines) => {
                Some(
                    n_lines
                        .parse::<usize>()
                        .map_err(|_| HeadError::InvalidNumLines {
                            n_lines: n_lines.to_owned(),
                        })?,
                )
            }
        };
        let n_bytes = match matches.value_of("n_bytes") {
            None => None,
            Some(n_bytes) => {
                Some(
                    n_bytes
                        .parse::<usize>()
                        .map_err(|_| HeadError::InvalidNumBytes {
                            n_bytes: n_bytes.to_owned(),
                        })?,
                )
            }
        };
        if n_lines.is_none() && n_bytes.is_none() {
            n_lines = Some(10)
        } else if n_lines.is_some() && n_bytes.is_some() {
            return Err(HeadError::InvalidCombination {}.into());
        }
        Ok(Self {
            files: matches
                .values_of("files")
                .unwrap()
                .map(|val| val.into())
                .collect(),
            n_lines,
            n_bytes,
        })
    }

    pub fn head(&self) -> HeadResult<String> {
        match self.files[0].as_ref() {
            "-" => {
                // Read from stdin
                let reader = BufReader::new(stdin());
                self.read_file(reader)
            }
            _ => match self.files.len() {
                1 => {
                    let reader = BufReader::new(File::open(Path::new(&self.files[0]))?);
                    let output = self.read_file(reader)?;
                    Ok(output)
                }
                _ => {
                    let mut buffer = String::new();
                    for file in &self.files {
                        buffer.push_str(&Self::make_banner(file));
                        match File::open(Path::new(&file)) {
                            Ok(file) => {
                                let reader = BufReader::new(file);
                                buffer.push_str(&self.read_file(reader)?);
                                buffer.push('\n');
                            }
                            Err(e) => eprintln!("{}: {}", file, e),
                        }
                    }
                    buffer.pop();
                    Ok(buffer)
                }
            },
        }
    }

    pub fn make_banner(file: &str) -> String {
        format!("==> {} <==\n", file)
    }

    fn read_file<T: Read>(&self, reader: BufReader<T>) -> HeadResult<String> {
        let mut counter: usize = 0;
        let mut is_done: Box<dyn FnMut(u8) -> bool> = if let Some(n_lines) = self.n_lines {
            Box::new(move |value| {
                if value as char == '\n' {
                    counter += 1;
                }
                counter >= n_lines
            })
        } else if let Some(n_bytes) = self.n_bytes {
            Box::new(move |_| {
                counter += 1;
                counter >= n_bytes
            })
        } else {
            unreachable!();
        };

        let mut buffer = Vec::<u8>::new();
        for byte in reader.bytes() {
            let byte = byte?;
            buffer.push(byte);
            if is_done(byte) {
                break;
            }
        }
        Ok(String::from_utf8_lossy(&buffer).to_string())
    }
}
