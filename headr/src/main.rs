fn main() {
    match headr::Config::get_args().and_then(|config| config.head()) {
        Ok(value) => print!("{}", value),
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(1);
        }
    }
}
