use std::fs::File;
use std::io::prelude::*;
use std::io::{stdin, BufReader};
use std::path::Path;

use clap::{App, Arg};

#[derive(Debug)]
pub struct Config {
    files: Vec<String>,
    number_lines: bool,
    number_nonblank_lines: bool,
}

#[derive(Debug)]
pub enum CatError {
    InvalidNumberOfArguments,
    FailedToReadFile,
}

type CatResult<T> = Result<T, CatError>;

impl Config {
    pub fn from_args() -> CatResult<Self> {
        let matches = App::new("catr")
            .arg(
                Arg::new("number_lines")
                    .short('n')
                    .long("number")
                    .takes_value(false),
            )
            .arg(
                Arg::new("number_nonblank_lines")
                    .short('b')
                    .long("number-nonblank")
                    .takes_value(false),
            )
            .arg(
                Arg::new("files")
                    .default_value("-")
                    .min_values(1),
            )
            .get_matches();
        Ok(Self {
            files: matches
                .values_of("files")
                .ok_or(CatError::InvalidNumberOfArguments)?
                .into_iter()
                .map(|s| s.to_string())
                .collect(),
            number_lines: matches.is_present("number_lines"),
            number_nonblank_lines: matches.is_present("number_nonblank_lines"),
        })
    }

    fn read_file(&self, file: &str) -> CatResult<String> {
        let mut buffer = String::new();
        let mut n_lines = 1;
        // Note: this is refered to as a "trait object". The same thing can be
        // achieved by using a &dyn T type to an object that implements a trait
        // T. Here it doesn't work because the object would be dropped when the
        // match's arm exits. I believe using Box is dynamic dispatching like
        // in cpp
        let reader: Box<dyn BufRead> = match file {
            "-" => Box::new(BufReader::new(stdin())),
            _ => Box::new(BufReader::new(
                File::open(Path::new(&file)).map_err(|_| CatError::FailedToReadFile)?,
            )),
        };
        for line in reader.lines() {
            let line = line.map_err(|_| CatError::FailedToReadFile)?;
            if (!line.is_empty() && self.number_nonblank_lines) || (self.number_lines) {
                buffer.push_str(&format!("{:>6}\t", n_lines));
                n_lines += 1;
            }
            buffer.push_str(&line);
            buffer.push('\n');
        }
        Ok(buffer)
    }

    pub fn cat(&self) -> String {
        let mut buffer = String::new();
        for file in &self.files {
            match self.read_file(file) {
                Ok(file_buffer) => buffer.push_str(&file_buffer),
                Err(_) => eprintln!("Failed to read file {}", file),
            }
        }
        buffer
    }
}

pub fn run() -> CatResult<()> {
    let config = Config::from_args()?;
    print!("{}", config.cat());
    Ok(())
}
